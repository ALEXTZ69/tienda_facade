﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PD_FACADE
{
    class SistemaAlmacen
    {
        private int cantidad;

        public SistemaAlmacen()
        {
            cantidad = 2;
        }

        public bool SacarAlmacen()
        {
            if (cantidad>0)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Producto Listo para Enviarse!");
                cantidad--;

                return true;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Producto no disponible, espere a que haya existencias!");

                return false;
            }
        }
    }
}
