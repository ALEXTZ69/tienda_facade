﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PD_FACADE
{
    class Fachada
    {
        private SistemaCompra compra = new SistemaCompra();
        private SistemaAlmacen almacen = new SistemaAlmacen();
        private SistemaEnvio envio = new SistemaEnvio();

        public void Comprar()
        {
            if (compra.Comprar())
            {
                if (almacen.SacarAlmacen())
                {
                    envio.EnviarPedido();
                }
            }
        }

    }
}
