﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PD_FACADE
{
    class SistemaCompra //Esta Clase representa un subsistema
    {
        public bool Comprar()
        {
            string dato="";
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Introducir Numero de Tarjeta: ");
            dato=Console.ReadLine();

            if (dato == "12345")
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Pago Aceptado!");

                return true;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Pago Rechazado!");

                return false;
            }
        }
    }
}
