﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Necesario para usar los metodos dentro de las clases
using PD_FACADE;

namespace PD_FACADE
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Fachada facade = new Fachada();

            facade.Comprar();
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("---------------");

            facade.Comprar();
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("---------------");

            facade.Comprar();
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("---------------");


            Console.ReadKey();
        }
    }
}
